# Source code of contrast-finder.com website

https://contrast-finder.com/

## QA Tools

### Online tools
`*` preconfigured tools

* Security
    * [Hardenize](https://www.hardenize.com) (DNS, SMTP, web server)
    * [Mozilla Observatory](https://observatory.mozilla.org/analyze.html?host=contrast-finder.com) `*` (HTTP header, SSL, cookies, ...)
    * [Security Headers](https://securityheaders.io/?q=https://contrast-finder.com) `*` (HTTP header)
    * Content-Security-Policy (CSP)
        * [cspvalidator.org](https://cspvalidator.org/#url=https://contrast-finder.com) `*` 
        * [csp-evaluator.withgoogle.com](https://csp-evaluator.withgoogle.com/?csp=https://contrast-finder.com) `*` 
    * SSL
        * [ssllabs.com](https://www.ssllabs.com/ssltest/analyze?d=contrast-finder.com) `*` 
        * [tls.imirhil.fr](https://tls.imirhil.fr/https/contrast-finder.com) `*` 
* W3C tools
    * [HTML validator](https://validator.w3.org/nu/?doc=https://contrast-finder.com&showsource=yes&showoutline=yes&showimagereport=yes) `*` 
    * [CSS validator](https://jigsaw.w3.org/css-validator/validator?uri=https://contrast-finder.com&profile=css3) `*` 
    * [i18n checker](https://validator.w3.org/i18n-checker/check?uri=https://contrast-finder.com) `*` 
    * [Link checker](https://validator.w3.org/checklink?uri=https://contrast-finder.com&hide_type=all&depth=&check=Check) `*` 
* Web accessibility
    * [Asqatasun](https://app.asqatasun.org)
* Web perf
    * [Yellowlab](http://yellowlab.tools)
    * [Webpagetest](https://www.webpagetest.org/)
    * [Test a single asset from 14 locations](https://tools.keycdn.com/performance?url=https://contrast-finder.com) `*`
* HTTP/2
    * [Http2.pro](https://http2.pro/check?url=https://contrast-finder.com/) `*` (check server HTTP/2, ALPN, and Server-push support)
* Global tools (webperf, accessibility, security, ...)
    * [Dareboost](https://www.dareboost.com)  (free trial)
    * [Sonarwhal](https://sonarwhal.com/scanner/)


### Open-source softwares

* [W3C tools](https://w3c.github.io/developers/tools/#tools) 
* Security
    * [Arachni](https://github.com/Arachni/arachni) (web application security scanner framework) 
    * Content-Security-Policy (CSP)
        * [salvation](https://github.com/shapesecurity/salvation) (Java parser, warn about policy errors)
    * Mozilla Observatory
        * [CLI client for Mozilla Observatory](https://github.com/mozilla/observatory-cli)
        * [HTTP Observatory](https://github.com/mozilla/http-observatory) (local scanner : CLI and CI)
* Web accessibility
    * Asqatasun
        * [Asqatsun Docker image](https://hub.docker.com/r/asqatasun/asqatasun/)
        * [Install Asqatasun on a server](https://doc.asqatasun.org/en/10_Install_doc/Asqatasun/)
* Web perf
    * Webpagetest
    * [Yellowlab](https://github.com/gmetais/YellowLabTools/) (API, npm CLI, Grunt task, ...) 
    * [Sitespeed.io](https://www.sitespeed.io/) (npm or docker is needed)
* Global tools
    * [Sonarwhal](https://github.com/sonarwhal/sonarwhal) (Node.js v8)




    


